package in.celso.atomys;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;

public class MainMenu extends JMenuBar {
	
	protected MainMenu() {
		JMenu menuFile = new JMenu("File");
		{
			CMenuItem newItem = new CMenuItem("New", "Ctrl+N");
			menuFile.add(newItem);
			
			CMenuItem openItem = new CMenuItem("Open...", "Ctrl+O");
			menuFile.add(openItem);
			
			CMenuItem saveItem = new CMenuItem("Save", "Ctrl+S");
			menuFile.add(saveItem);
			
			CMenuItem saveAsItem = new CMenuItem("Save as...", null);
			menuFile.add(saveAsItem);
		}
		add(menuFile);
		
		JMenu menuEdit = new JMenu("Edit");
		{
			CMenuItem undoItem = new CMenuItem("Undo", "Ctrl+Z");
			menuEdit.add(undoItem);
			
			CMenuItem redoItem = new CMenuItem("Redo", "Ctrl+Y");
			menuEdit.add(redoItem);
		}
		add(menuEdit);
		
		JMenu menuOptions = new JMenu("Options");
		{
			CMenuItem setsItem = new CMenuItem("General preferences", "Ctrl+O");
			menuOptions.add(setsItem);
			
			CMenuItem prefsItem = new CMenuItem("Graphics preferences", null);
			menuOptions.add(prefsItem);
			
			CMenuItem keysItem = new CMenuItem("Key bindings", null);
			menuOptions.add(keysItem);
			
			JMenu menuSkins = new JMenu("Skin");
			{
				ButtonGroup skinsGroup = new ButtonGroup();
				
				for (final Skin skin : Skin.skinList) {
					JRadioButtonMenuItem skinButton = new JRadioButtonMenuItem(skin.displayName);
					skinButton.setSelected(Atomys.getSkin() == skin);
					skinsGroup.add(skinButton);
					menuSkins.add(skinButton);
					skinButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Atomys.setSkin(skin);
							SwingUtilities.updateComponentTreeUI(Atomys.getInstance().getMainFrame());
						}
					});
				}
				
				menuSkins.add(new JSeparator());
				menuSkins.add(new JLabel("-JTattoo skins-", JLabel.CENTER));
				
				for (final Skin skin : Skin.jtatooSkinList) {
					JRadioButtonMenuItem skinButton = new JRadioButtonMenuItem(skin.displayName);
					skinButton.setSelected(Atomys.getSkin() == skin);
					skinsGroup.add(skinButton);
					menuSkins.add(skinButton);
					skinButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Atomys.setSkin(skin);
							SwingUtilities.updateComponentTreeUI(Atomys.getInstance().getMainFrame());
						}
					});
				}
			}
			menuOptions.add(menuSkins);
		}
		add(menuOptions);
		
		JMenu menuHelp = new JMenu("Help");
		{
			CMenuItem helpItem = new CMenuItem("Help content", "F1");
			menuHelp.add(helpItem);
			
			CMenuItem tipsItem = new CMenuItem("Tips & Tricks", null);
			menuHelp.add(tipsItem);
			
			menuHelp.add(new JSeparator());
			
			CMenuItem aboutItem = new CMenuItem("About", null);
			menuHelp.add(aboutItem);
		}
		add(menuHelp);
	}

}
