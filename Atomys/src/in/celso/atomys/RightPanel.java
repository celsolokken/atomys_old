package in.celso.atomys;

import java.awt.*;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;

public class RightPanel extends JPanel {

	private JTree partsTree;
	private JScrollPane treePane;
	
	private TexturePanel texturePanel; 

	public RightPanel(Model model) {
		
		texturePanel = new TexturePanel(model);
		partsTree = model.partsTree;
		partsTree.putClientProperty("JTree.lineStyle", "Angled");
		partsTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		partsTree.setCellRenderer(new TreeCellRenderer());
		
		treePane = new JScrollPane(partsTree);
		treePane.setPreferredSize(new Dimension(getWidth(), 150));

		//setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 0.0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(treePane, c);
		
		c.gridy++;
		add(positionPanel(), c);

		c.gridy++;
		add(sizePanel(), c);
		
		c.gridy++;
		add(offsetPanel(), c);
		
		c.gridy++;
		add(textureOffsetPanel(), c);
		
		c.gridy++;
		add(rotationPanel(), c);
		
		JScrollPane textureView = new JScrollPane(texturePanel);
		textureView.setViewportBorder(null);
		
		if (model.textureHeight > 128)
			textureView.setPreferredSize(new Dimension(getWidth(), 128));
		else
			textureView.setPreferredSize(new Dimension(getWidth(), textureView.getPreferredSize().height + 20));
		
		JPanel textureWrapper = new JPanel();
		textureWrapper.setBorder(BorderFactory.createEtchedBorder());
		textureWrapper.setLayout(new BoxLayout(textureWrapper, BoxLayout.Y_AXIS));
		textureWrapper.add(new JLabel("Texture"));
		textureWrapper.add(textureView);

		c.weighty = 1.0;
		c.gridy++;
		add(textureWrapper, c);
		
		{
			model.addPart(new Part("Cabeça"));
			model.addPart(new Part("Ombro"));
			Part joelho = new Part("Joelho");
			model.addPart(joelho);
			model.addPart(new Part("Pé"), joelho);
		}
	}
	
	private JPanel positionPanel() {
		JPanel jp = new JPanel();
		jp.setLayout(new GridBagLayout());
		jp.setBorder(BorderFactory.createEtchedBorder());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 1.0;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		jp.add(new JLabel("Position"), c);
		
		c.gridwidth = 1;
		c.gridy++;
		JSpinner posX = new JSpinner();
		jp.add(posX, c);
		
		c.gridx++;
		JSpinner posY = new JSpinner();
		jp.add(posY, c);
		
		c.gridx++;
		JSpinner posZ = new JSpinner();
		jp.add(posZ, c);
		
		return jp;
	}
	
	private JPanel sizePanel() {
		JPanel jp = new JPanel();
		jp.setLayout(new GridBagLayout());
		jp.setBorder(BorderFactory.createEtchedBorder());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 1.0;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		jp.add(new JLabel("Dimension"), c);
		
		c.gridwidth = 1;
		c.gridy++;
		JSpinner sizeX = new JSpinner();
		jp.add(sizeX, c);
		
		c.gridx++;
		JSpinner sizeY = new JSpinner();
		jp.add(sizeY, c);
		
		c.gridx++;
		JSpinner sizeZ = new JSpinner();
		jp.add(sizeZ, c);
		
		return jp;
	}
	
	private JPanel offsetPanel() {
		JPanel jp = new JPanel();
		jp.setLayout(new GridBagLayout());
		jp.setBorder(BorderFactory.createEtchedBorder());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 1.0;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		jp.add(new JLabel("Offset"), c);
		
		c.gridwidth = 1;
		c.gridy++;
		JSpinner offX = new JSpinner();
		jp.add(offX, c);
		
		c.gridx++;
		JSpinner offY = new JSpinner();
		jp.add(offY, c);
		
		c.gridx++;
		JSpinner offZ = new JSpinner();
		jp.add(offZ, c);
		
		return jp;
	}
	
	private JPanel textureOffsetPanel() {
		JPanel jp = new JPanel();
		jp.setLayout(new GridBagLayout());
		jp.setBorder(BorderFactory.createEtchedBorder());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 1.0;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		jp.add(new JLabel("Texture Offset"), c);
		
		c.gridwidth = 1;
		c.gridy++;
		JSpinner offX = new JSpinner();
		jp.add(offX, c);
		
		c.gridx++;
		JSpinner offY = new JSpinner();
		jp.add(offY, c);
		
		return jp;
	}
	
	private JPanel rotationPanel() {
		JPanel jp = new JPanel();
		jp.setLayout(new GridBagLayout());
		jp.setBorder(BorderFactory.createEtchedBorder());
		
		JScrollBar rotX = new JScrollBar(JScrollBar.HORIZONTAL);
		JScrollBar rotY = new JScrollBar(JScrollBar.HORIZONTAL);
		JScrollBar rotZ = new JScrollBar(JScrollBar.HORIZONTAL);
		
		JSpinner rotXS = new JSpinner();
		JSpinner rotYS = new JSpinner();
		JSpinner rotZS = new JSpinner();
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weightx = 1.0;
        c.weighty = 1.0;
		c.gridwidth = 5;
		c.gridheight = 1;
		c.gridx = 0;
		c.gridy = 0;
		
		jp.add(new JLabel("Rotations"), c);

		c.gridwidth = 1;
		c.gridy++;
		c.weightx = 0.2;
		jp.add(rotXS, c);
		
		c.gridx++;
		c.gridwidth = 4;
		c.weightx = 0.8;
		jp.add(rotX, c);

		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;
		c.weightx = 0.2;
		jp.add(rotYS, c);
		
		c.gridx++;
		c.gridwidth = 4;
		c.weightx = 0.8;
		jp.add(rotY, c);
		
		c.gridy++;
		c.gridx = 0;
		c.gridwidth = 1;
		c.weightx = 0.2;
		jp.add(rotZS, c);
		
		c.gridx++;
		c.gridwidth = 4;
		c.weightx = 0.8;
		jp.add(rotZ, c);
		
		return jp;
	}

	private class TreeCellRenderer extends DefaultTreeCellRenderer {
		
		private final ImageIcon LEAF_ICON = Utils.scale(new ImageIcon(Atomys.HOME + "/res/part.png"), 0.5, partsTree);
		private final ImageIcon BRANCH_ICON = Utils.scale(new ImageIcon(Atomys.HOME + "/res/part_2.png"), 0.5, partsTree);

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {

			setIcon(row == 0 ? null : leaf ? LEAF_ICON : BRANCH_ICON);
			
			setFont(Settings.font);
			
			setText( ((Part) value).name );
			
			return this;
		}
	}

	private class PartsModel extends DefaultTreeModel {

		public PartsModel(TreeNode root) {
			super(root);
		}

	}

}
