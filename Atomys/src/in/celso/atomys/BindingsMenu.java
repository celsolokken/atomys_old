package in.celso.atomys;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class BindingsMenu extends JDialog {
	
	private Map<String, Vector<String>> bindings = new HashMap<>();
	
	private JTable table = new JTable();
	
	protected BindingsMenu() {
		super(Atomys.getInstance().getMainFrame());
	}
	
	protected void setKeyBinding(String cmd, String... bindings) {
		Vector<String> v = new Vector<>(bindings.length);
		
		for (String s : bindings)
			v.add(s);
		
		this.bindings.put(cmd, v);
	}
	
	protected Vector<String> getKeyBinding(String cmd) {
		return this.bindings.get(cmd);
	}
	
	protected void display() {
		
	}
	
}
