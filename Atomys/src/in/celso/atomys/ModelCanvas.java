package in.celso.atomys;

import java.awt.Canvas;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluPerspective;

public class ModelCanvas extends Canvas implements ComponentListener {

	private static final long serialVersionUID = -6484855085473769177L;
	
	private boolean isResized;
	private boolean isStarted;

	protected ModelCanvas() {
		super();
		addComponentListener(this);
	}
	
	public synchronized boolean isStarted() {
		return isStarted;
	}
	
	public synchronized boolean isResized() {
		return isResized;
	}
	
	public synchronized void setResized(boolean isResized) {
		this.isResized = isResized;
	}

	public void run() {
		try {
			Display.setParent(this);
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			//fatal error
		}

		initGL();
		getDelta();
		lastFPS = getTime();
		
		isStarted = true;
		
		while (!Display.isCloseRequested()) {
			int delta = getDelta();

			update(delta);
			renderGL();

			Display.update();
			Display.sync(30);
		}

		Display.destroy();
	}
	
	protected void resize() {
		System.out.println("Resize called on model");
		glViewport(0, 0, getWidth(), getHeight());
		update3D();
	}

	private void update(int delta) {
		if (isResized()) {
			resize();
			setResized(false);
		}
		updateFPS();
	}
	
	private void renderGL() {
		
	}
	
	private void initGL() {
		update3D();
        glEnable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		System.out.println("initGL() called");
	}
	
	private void update3D() {
		glMatrixMode(GL11.GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(45.0f, ((float)getWidth())/((float)getHeight()), 0.1f, 100.0f);
        glMatrixMode(GL11.GL_MODELVIEW);
        glLoadIdentity();
	}
	
	private long lastFrame;
	private int fps;
	private long lastFPS;

	private int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;

		return delta;
	}

	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	private void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("FPS: " + fps);
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}

	public void componentResized(ComponentEvent e) {
		setResized(true);
	}

	public void componentMoved(ComponentEvent e) {}

	public void componentShown(ComponentEvent e) {}

	public void componentHidden(ComponentEvent e) {}

}
