package in.celso.atomys;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.html.HTMLDocument;

public class Console extends JPanel {

	private JEditorPane textPane;
	
	private JTextField inputField;
	
	private final int COMMAND = 0, COMMENT = 1, ERROR = 2, LOG = 3, MSG = 4;
	
	protected Console() {
		textPane = new JEditorPane();
		inputField = new JTextField();
		
		setLayout(new BorderLayout());
		
		JScrollPane textView = new JScrollPane(textPane);
		textView.setAutoscrolls(true);
		
		add(textView, BorderLayout.CENTER);
		add(inputField, BorderLayout.PAGE_END);
		
		textPane.setContentType("text/html");
		textPane.setEditable(false);
		
		logMessage("Welcome to Atomys v" + Atomys.VERSION, MSG);
		
		inputField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cmd = inputField.getText();
				if (cmd.length() > 0) {
					inputField.setText("");
					parseCommand(cmd);
				}
			}
		});
	}
	
	private void logMessage(String msg, int type) {
		HTMLDocument doc = (HTMLDocument) textPane.getDocument();
		String preffix = "", suffix = "";
		Color color = null;
		
		switch (type) {
		case COMMAND:
			preffix = "> ";
			color = Settings.commandColor;
			break;
		case COMMENT:
			suffix = preffix = "\"";
			color = Settings.commentColor;
			break;
		case ERROR:
			preffix = "[Error] { ";
			suffix = " }";
			color = Settings.errorColor;
			break;
		case LOG:
			preffix = "(";
			suffix = ")";
			color = Settings.logColor;
			break;
		case MSG:
			color = Settings.msgColor;
		}
		
		StyleContext sc = StyleContext.getDefaultStyleContext(); 
	    AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);
	    aset = sc.addAttribute(aset, StyleConstants.FontFamily, Settings.font.getFamily());
	    aset = sc.addAttribute(aset, StyleConstants.FontSize, Settings.fontSize);
	    aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
		
		try {
			doc.insertString(doc.getLength(), preffix + msg + suffix + "\n", aset);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
		textPane.repaint();
	}
	
	private void parseCommand(String cmd) {
		if (!cmd.startsWith("/")) {
			logMessage(cmd, COMMENT);
			return;
		}
	}

}
