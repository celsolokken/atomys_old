package in.celso.atomys;

import static org.lwjgl.opengl.GL11.*;

import java.util.List;

public class ModelRender {
	
	public static final int CENTERED_CAM = 0, FREE_CAM = 1;
	
	public static final int SIMPLE_LIGHT = 0, FULL_LIGHT = 1, SKY_LIGHT = 2, CAM_LIGHT = 3;
	
	private Model model;
	
	private boolean showBlock = true;
	
	protected ModelRender(Model model) {
		this.model = model;
	}
	
	public void render() {
		if (showBlock)
			renderBlock();
		
		List<Part> children = ((Part) model.partsTree.getModel().getRoot()).children;
		for (Part child : children)
			drawPart(child);
	}
	
	private void drawPart(Part part) {
		glPushMatrix();
		{	
			glTranslatef(part.posX, part.posY, part.posZ);
			
			//draws
			
			List<Part> children = part.children;
			if (children != null && !children.isEmpty()) {
				for (Part child : children)
					drawPart(child);
			}
		}
		glPopMatrix();
	}
	
	private void renderBlock() {
		
	}
	
}
