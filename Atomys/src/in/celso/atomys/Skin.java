package in.celso.atomys;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.UIManager;

public class Skin {

	public String path;
	public String displayName;
	
	public static List<Skin> skinList = new ArrayList<>();
	public static List<Skin> jtatooSkinList = new ArrayList<>();

	public static final Skin SYSTEM = new Skin(UIManager.getSystemLookAndFeelClassName(), "System (Default)");
	static {
		skinList.add(SYSTEM);
	}

	public Skin(String path, String displayName) {
		this.path = path;
		this.displayName = displayName;
	}

	protected static void loadSkins() {
		try {
			Properties props = new Properties();
			props.put("logoString", "");
			
			addJTattooLAF("com.jtattoo.plaf.smart.SmartLookAndFeel", "Smart", props);
			addJTattooLAF("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel", "Aluminium", props);
			addJTattooLAF("com.jtattoo.plaf.acryl.AcrylLookAndFeel", "Acryl", props);
			addJTattooLAF("com.jtattoo.plaf.aero.AeroLookAndFeel", "Aero", props);
			//addJTattooLAF("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel", "Bernstein", props);
			addJTattooLAF("com.jtattoo.plaf.graphite.GraphiteLookAndFeel", "Graphite", props);
			addJTattooLAF("com.jtattoo.plaf.fast.FastLookAndFeel", "Fast", props);
			addJTattooLAF("com.jtattoo.plaf.hifi.HiFiLookAndFeel", "Hi-Fi", props);
			addJTattooLAF("com.jtattoo.plaf.luna.LunaLookAndFeel", "Luna", props);
			addJTattooLAF("com.jtattoo.plaf.mcwin.McWinLookAndFeel", "McWin", props);
			addJTattooLAF("com.jtattoo.plaf.mint.MintLookAndFeel", "Mint", props);
			addJTattooLAF("com.jtattoo.plaf.noire.NoireLookAndFeel", "Noire", props);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void addJTattooLAF(String path, String displayName, Properties props) throws Exception {
		Class.forName(path).getMethod("setTheme", Properties.class).invoke(null, props);
		jtatooSkinList.add(new Skin(path, displayName));
	}
	

	@Override
	public String toString() {
		return displayName + " [" + path + "]";
	}

}
