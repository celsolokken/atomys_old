package in.celso.atomys;

import java.awt.Color;
import java.awt.Font;

public class Settings {
	
	public static int windowWidth = 900, windowHeight = 700;
	boolean advancedLight = false;
	public static String fontName = Font.DIALOG;
	
	public static int fontSize = 12;
	
	public static Font font = new Font(fontName, Font.PLAIN, fontSize);
	
	public static Color commandColor = Color.BLACK, commentColor = new Color(140, 140, 140), errorColor = Color.RED,
						logColor = new Color(20, 240, 32), msgColor = new Color(10, 110, 180);
	
	public static int cameraType = ModelRender.CENTERED_CAM;
	
	public static int blockID = 0;
	
	public static boolean raypickOn = false;
	
	public static int lightMode = ModelRender.SIMPLE_LIGHT;
	
	public static int[] backgroundColor = new int[] {121, 131, 138};
	
	public static int[] defaultPartColor = new int[] {175, 175, 175};

}
