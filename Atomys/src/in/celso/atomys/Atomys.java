package in.celso.atomys;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

public class Atomys {
	
	public static final String VERSION = "0.1.0";
	
	public static final String MC_VERSION = "1.7.5";
	
	public static final String HOME = getHome();
	
	public static final File HOME_FOLDER = new File(HOME);
	
	private static Skin currentSkin;
	
	static {
		if (!HOME_FOLDER.exists())
			HOME_FOLDER.mkdir();
	}
	
	private static Atomys instance;
	
	private Model model = new Model("Modelo", MC_VERSION);//TODO remover inicializacao
	
	private JFrame mainFrame;
	private JPanel mainPanel;
	private JTabbedPane bottomPane;
	private RightPanel rightPanel;
	private JSplitPane hSplit, vSplit;
	
	private Console console;
	private ModelControlPanel modelCPanel;
	
	protected ModelCanvas modelCanvas;
	private Thread modelThread;
	
	protected static synchronized Atomys getInstance() {
		if (instance == null)
			return instance = new Atomys();
		return instance;
	}
	
	private Atomys() {
		setSkin(Skin.SYSTEM);
		//Skin.loadSkins();
		
		//Utils.setUIFont(new FontUIResource(Settings.font));
		
		mainFrame = new JFrame("Atomys v" + VERSION);
		mainFrame.setSize(Settings.windowWidth, Settings.windowHeight);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // remover
		
		mainFrame.addWindowListener(getWindowListener());
		mainFrame.addComponentListener(getComponentListener());
		
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);
		mainFrame.setJMenuBar(new MainMenu());
		
		populate();
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				mainFrame.setVisible(true);
			}
		});
		/*if (JOptionPane.showConfirmDialog(mainFrame, "Criar?") == 0) {
			modelCanvas.run();
		}*/
	}
	
	private void populate() {
		console = new Console();
		modelCPanel = new ModelControlPanel();
		
		mainPanel = new JPanel();
		rightPanel = new RightPanel(model);
		bottomPane = new JTabbedPane();
		bottomPane.addTab("Control Panel", modelCPanel);
		bottomPane.addTab("Console", console);
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(modelCanvas = new ModelCanvas());
		
		vSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, mainPanel, bottomPane);
		hSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, vSplit, new JScrollPane(rightPanel));
		
		vSplit.setEnabled(false);
		hSplit.setEnabled(false);
		
		mainFrame.add(hSplit);
	}
	
	private final WindowListener getWindowListener() {
		return new WindowListener() {
			public void windowOpened(WindowEvent e) {}

			public void windowClosing(WindowEvent e) {
				//wait are you sure you wanna close?
			}
			
			public void windowClosed(WindowEvent e) {}

			public void windowIconified(WindowEvent e) {}

			public void windowDeiconified(WindowEvent e) {}

			public void windowActivated(WindowEvent e) {}

			public void windowDeactivated(WindowEvent e) {}
			
		};
	}
	
	private final ComponentListener getComponentListener() {
		return new ComponentListener() {
			public void componentResized(ComponentEvent e) {
				System.out.println("frame resized");
				vSplit.setDividerLocation(mainFrame.getHeight() - 180);
				hSplit.setDividerLocation(mainFrame.getWidth() - 200);
			}

			public void componentMoved(ComponentEvent e) {}
			
			public void componentShown(ComponentEvent e) {}
			
			public void componentHidden(ComponentEvent e) {}
		};
	}
	
	protected synchronized JFrame getMainFrame() {
		return mainFrame;
	}
	
	public static synchronized void setSkin(Skin skin) {
		currentSkin = skin;
		synchronized (currentSkin) {
			try {
				UIManager.setLookAndFeel(skin.path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static synchronized Skin getSkin() {
		synchronized (currentSkin) {
			return currentSkin;
		}
	}
	
	private static final String getHome() {
	    String OS = System.getProperty("os.name").toUpperCase();
	    String s = "";
	    if (OS.contains("WIN"))
	        s =  System.getenv("APPDATA");
	    else if (OS.contains("MAC"))
	        s =  System.getProperty("user.home") + "/Library/Application Support";
	    else if (OS.contains("NUX"))
	        s = System.getProperty("user.home");
	    else
	    	s = System.getProperty("user.dir");
	    
	    s = s + "/atomys";
	    
	    return s;
	}
	
	public static void main(String... args) {
		Atomys.getInstance();
	}

}
