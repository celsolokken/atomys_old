package in.celso.atomys;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class Utils {

	public static BufferedImage createImage(String path) throws IOException {
		if (path.startsWith("~"))
			return ImageIO.read(Utils.class.getClassLoader()
					.getResourceAsStream(path.substring(1)));
		return ImageIO.read(new File(path));
	}

	public static ImageIcon scale(ImageIcon icon, double scaleFactor,
			Component comp) {
		int width = icon.getIconWidth();
		int height = icon.getIconHeight();

		width = (int) Math.ceil(width * scaleFactor);
		height = (int) Math.ceil(height * scaleFactor);

		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = image.createGraphics();
		g.scale(scaleFactor, scaleFactor);
		icon.paintIcon(comp, g, 0, 0);
		g.dispose();

		return new ImageIcon(image);
	}

	public static void setUIFont(javax.swing.plaf.FontUIResource f) {
		java.util.Enumeration keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value != null && value instanceof FontUIResource) {
				UIManager.put(key, f);
				//System.out.println(key + " " + f);
			}
		}
	}

}
